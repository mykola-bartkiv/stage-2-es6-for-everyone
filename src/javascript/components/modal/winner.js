import { showModal } from './modal';
import { createElement } from '../../helpers/domHelper';
import App from '../../app';

export function showWinnerModal(fighter) {
  const bodyElement = createElement({ tagName: 'p' });

  bodyElement.innerText = 'Сlose the window to repeat.'

  showModal({
    title: `Congratulations!!! ${fighter.name} won!`,
    bodyElement,
    onClose: retry,
  });
}

function retry() {
  const rootElement = document.getElementById('root');

  rootElement.innerHTML = '';
  new App();
}
