import { createElement } from '../helpers/domHelper';

export function createFighterPreview(fighter, position) {
  const positionClassName = position === 'right' ? 'fighter-preview___right' : 'fighter-preview___left';
  const fighterElement = createElement({
    tagName: 'div',
    className: `fighter-preview___root ${positionClassName}`,
  });
  const fighterDetals = [];

  for (const property in fighter) {
    if (!fighter.hasOwnProperty(property)) continue;
    if (property === '_id') continue;
    if (property === 'source') {
      const image = createFighterImage(fighter);
      fighterDetals.push(image);
      continue;
    }

    const propertyTitle = createElement({
      tagName: 'strong',
    });
    const fighterProperty = createElement({
      tagName: 'p',
      className: `fighter-property fighter-${property}`,
    });

    propertyTitle.innerText = `${property}:`;
    fighterProperty.innerText = fighter[property];
    fighterProperty.prepend(propertyTitle);
    fighterDetals.push(fighterProperty);
  }

  fighterElement.append(...fighterDetals);

  return fighterElement;
}

export function createFighterImage(fighter) {
  const { source, name } = fighter;
  const attributes = {
    src: source,
    title: name,
    alt: name,
  };
  const imgElement = createElement({
    tagName: 'img',
    className: 'fighter-preview___img',
    attributes,
  });

  return imgElement;
}
