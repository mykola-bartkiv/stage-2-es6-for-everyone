import { controls } from '../../constants/controls';

export async function fight(firstFighter, secondFighter) {
  return new Promise((resolve) => {
    const {
      PlayerOneAttack,
      PlayerOneBlock,
      PlayerTwoAttack,
      PlayerTwoBlock,
      PlayerOneCriticalHitCombination,
      PlayerTwoCriticalHitCombination,
    } = controls;
    const useCriticalHit = {
      leftFighter: true,
      rightFighter: true,
    };
    const TIME_OUT = 10e3;
    let winner = null;
    const map = {};

    window.addEventListener('keydown', onkeydown);
    window.addEventListener('keyup', onkeydown);

    function onkeydown(event) {
      map[event.code] = event.type == 'keydown';

      const playerOneCriticalCombination =
        map[PlayerOneCriticalHitCombination[0]] &&
        map[PlayerOneCriticalHitCombination[1]] &&
        map[PlayerOneCriticalHitCombination[2]];
      const playerTwoCriticalCombination =
        map[PlayerTwoCriticalHitCombination[0]] &&
        map[PlayerTwoCriticalHitCombination[1]] &&
        map[PlayerTwoCriticalHitCombination[2]];
      const playerOneAttack = map[PlayerOneAttack] && !map[PlayerTwoBlock] && !map[PlayerOneBlock];
      const playerTwoAttack = map[PlayerTwoAttack] && !map[PlayerOneBlock] && !map[PlayerTwoBlock];

      if (playerOneCriticalCombination && playerTwoCriticalCombination) {
        winner = fighterHit('left', true);
        winner = fighterHit('right', true);
      } else if (playerOneCriticalCombination) {
        winner = fighterHit('left', true);
      } else if (playerTwoCriticalCombination) {
        winner = fighterHit('right', true);
      } else if (playerOneAttack) {
        winner = fighterHit('left');
      } else if (playerTwoAttack) {
        winner = fighterHit('right');
      }

      if (winner) {
        resolve(winner);
      }
    }

    function fighterHit(attackingFighter, critical = false) {
      if (winner !== null) return winner;
      const attacker = attackingFighter === 'left' ? firstFighter : secondFighter;
      const defender = attackingFighter === 'left' ? secondFighter : firstFighter;
      const defenderSide = attackingFighter === 'left' ? 'right' : 'left';
      const defenderElement = document.getElementById(`${defenderSide}-fighter-indicator`);
      const damage = getDamage(attacker, defender);
      let healthIndicator = parseFloat(defenderElement.style.width) || 100;
      const fighter = attackingFighter === 'left' ? 'leftFighter' : 'rightFighter';

      if (critical && useCriticalHit[fighter]) {
        healthIndicator -= (attacker.attack * 2 * 100) / defender.health;
        useCriticalHit[fighter] = false;
        setTimeout(function () {
          useCriticalHit[fighter] = true;
        }, TIME_OUT);
      } else if (!critical) {
        healthIndicator -= (damage * 100) / defender.health;
      }

      defenderElement.style.width = `${healthIndicator <= 0 ? 0 : healthIndicator}%`;

      return healthIndicator <= 0 ? attacker : null;
    }
  });
}

export function getDamage(attacker, defender) {
  const attackPower = getHitPower(attacker);
  const blockPower = getBlockPower(defender);
  const damange = attackPower - blockPower;

  return damange > 0 ? damange : 0;
}

export function getHitPower(fighter) {
  const MIN = 1;
  const MAX = 2;
  const criticalHitChance = Math.random() * (MAX - MIN) + MIN;

  return fighter.attack * criticalHitChance;
}

export function getBlockPower(fighter) {
  const MIN = 1;
  const MAX = 2;
  const dodgeChance = Math.random() * (MAX - MIN) + MIN;

  return fighter.defense * dodgeChance;
}
